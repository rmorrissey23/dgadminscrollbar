// Set the admin menu scrollbar max-height and overflow-y style parameters
function dgAdminMenuScrollbar() {
	var admin_menu = document.getElementById('admin-menu-link').getElementsByTagName('ul')[0];
	admin_menu.style.maxHeight = '500px';
	admin_menu.style.overflowY = 'auto';
}

dgAdminMenuScrollbar();
