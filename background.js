// Show page action icon in omnibar.
function showPageAction(tabId, changeInfo, tab) {
	// matches any dicomgrid.com URL
	var pattern = /https:\/\/.*.dicomgrid.com.*/g;

	// If the tab URL is a valid DICOM Grid URL...
	if (tab.url.match(pattern)) {
		chrome.pageAction.show(tabId);
	}
}

// Call the above function when the url of a tab changes.
chrome.tabs.onUpdated.addListener(showPageAction);
