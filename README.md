# _DG Admin Scrollbar_

_Description: A Google Chrome browser extension that applies a vertical scrollbar to the Administration menu in DG._

## How To

### Instructions for Use

* Download the and unzip the [DGAdminScrollbar.zip](https://bitbucket.org/rmorrissey23/dgadminscrollbar/downloads/DGAdminScrollbar.zip) file and store is locally on your machine
* Open Google Chrome and navigate to: Tools > Extensions
* Enable _Developer mode_ by checking the box at the top of the list
* Choose _Load unpacked extension..._ and locate the folder you just unzipped
* Navigate to any DG page and click on the _Administration_ menu to get a vertical scrollbar
* You should see a DG logo in the browser address bar which means the extension is running (click to verify)

## Credits

CSS snippets taken from [Twitter Bootstrap 3 RC1](https://github.com/twitter/bootstrap) source.

## Change Log

### v1.0

* Initial commit
